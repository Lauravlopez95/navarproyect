/****************************************************************************
** Meta object code from reading C++ file 'NavarQT.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "NavarQT.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'NavarQT.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_NavarQT_t {
    QByteArrayData data[27];
    char stringdata0[560];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_NavarQT_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_NavarQT_t qt_meta_stringdata_NavarQT = {
    {
QT_MOC_LITERAL(0, 0, 7), // "NavarQT"
QT_MOC_LITERAL(1, 8, 15), // "createLabelLeft"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 9), // "imgSource"
QT_MOC_LITERAL(4, 35, 16), // "createLabelRight"
QT_MOC_LITERAL(5, 52, 16), // "startCalibration"
QT_MOC_LITERAL(6, 69, 11), // "startServer"
QT_MOC_LITERAL(7, 81, 15), // "colorLabelGreen"
QT_MOC_LITERAL(8, 97, 13), // "colorLabelRed"
QT_MOC_LITERAL(9, 111, 7), // "doFlash"
QT_MOC_LITERAL(10, 119, 23), // "on_pushButton_1_clicked"
QT_MOC_LITERAL(11, 143, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(12, 167, 23), // "on_pushButton_3_clicked"
QT_MOC_LITERAL(13, 191, 23), // "on_pushButton_4_clicked"
QT_MOC_LITERAL(14, 215, 23), // "on_pushButton_5_clicked"
QT_MOC_LITERAL(15, 239, 23), // "on_pushButton_6_clicked"
QT_MOC_LITERAL(16, 263, 23), // "on_pushButton_7_clicked"
QT_MOC_LITERAL(17, 287, 23), // "on_pushButton_8_clicked"
QT_MOC_LITERAL(18, 311, 23), // "on_pushButton_9_clicked"
QT_MOC_LITERAL(19, 335, 24), // "on_pushButton_10_clicked"
QT_MOC_LITERAL(20, 360, 27), // "on_pushButton_close_clicked"
QT_MOC_LITERAL(21, 388, 27), // "on_pushButton_tibia_clicked"
QT_MOC_LITERAL(22, 416, 27), // "on_pushButton_femur_clicked"
QT_MOC_LITERAL(23, 444, 28), // "on_pushButton_step_1_clicked"
QT_MOC_LITERAL(24, 473, 28), // "on_pushButton_step_2_clicked"
QT_MOC_LITERAL(25, 502, 28), // "on_pushButton_step_3_clicked"
QT_MOC_LITERAL(26, 531, 28) // "on_pushButton_step_4_clicked"

    },
    "NavarQT\0createLabelLeft\0\0imgSource\0"
    "createLabelRight\0startCalibration\0"
    "startServer\0colorLabelGreen\0colorLabelRed\0"
    "doFlash\0on_pushButton_1_clicked\0"
    "on_pushButton_2_clicked\0on_pushButton_3_clicked\0"
    "on_pushButton_4_clicked\0on_pushButton_5_clicked\0"
    "on_pushButton_6_clicked\0on_pushButton_7_clicked\0"
    "on_pushButton_8_clicked\0on_pushButton_9_clicked\0"
    "on_pushButton_10_clicked\0"
    "on_pushButton_close_clicked\0"
    "on_pushButton_tibia_clicked\0"
    "on_pushButton_femur_clicked\0"
    "on_pushButton_step_1_clicked\0"
    "on_pushButton_step_2_clicked\0"
    "on_pushButton_step_3_clicked\0"
    "on_pushButton_step_4_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_NavarQT[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  134,    2, 0x0a /* Public */,
       4,    1,  137,    2, 0x0a /* Public */,
       5,    0,  140,    2, 0x0a /* Public */,
       6,    0,  141,    2, 0x0a /* Public */,
       7,    0,  142,    2, 0x0a /* Public */,
       8,    0,  143,    2, 0x0a /* Public */,
       9,    0,  144,    2, 0x0a /* Public */,
      10,    0,  145,    2, 0x0a /* Public */,
      11,    0,  146,    2, 0x0a /* Public */,
      12,    0,  147,    2, 0x0a /* Public */,
      13,    0,  148,    2, 0x0a /* Public */,
      14,    0,  149,    2, 0x0a /* Public */,
      15,    0,  150,    2, 0x0a /* Public */,
      16,    0,  151,    2, 0x0a /* Public */,
      17,    0,  152,    2, 0x0a /* Public */,
      18,    0,  153,    2, 0x0a /* Public */,
      19,    0,  154,    2, 0x0a /* Public */,
      20,    0,  155,    2, 0x0a /* Public */,
      21,    0,  156,    2, 0x0a /* Public */,
      22,    0,  157,    2, 0x0a /* Public */,
      23,    0,  158,    2, 0x0a /* Public */,
      24,    0,  159,    2, 0x0a /* Public */,
      25,    0,  160,    2, 0x0a /* Public */,
      26,    0,  161,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::QImage,    3,
    QMetaType::Void, QMetaType::QImage,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void NavarQT::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        NavarQT *_t = static_cast<NavarQT *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->createLabelLeft((*reinterpret_cast< const QImage(*)>(_a[1]))); break;
        case 1: _t->createLabelRight((*reinterpret_cast< const QImage(*)>(_a[1]))); break;
        case 2: _t->startCalibration(); break;
        case 3: _t->startServer(); break;
        case 4: _t->colorLabelGreen(); break;
        case 5: _t->colorLabelRed(); break;
        case 6: _t->doFlash(); break;
        case 7: _t->on_pushButton_1_clicked(); break;
        case 8: _t->on_pushButton_2_clicked(); break;
        case 9: _t->on_pushButton_3_clicked(); break;
        case 10: _t->on_pushButton_4_clicked(); break;
        case 11: _t->on_pushButton_5_clicked(); break;
        case 12: _t->on_pushButton_6_clicked(); break;
        case 13: _t->on_pushButton_7_clicked(); break;
        case 14: _t->on_pushButton_8_clicked(); break;
        case 15: _t->on_pushButton_9_clicked(); break;
        case 16: _t->on_pushButton_10_clicked(); break;
        case 17: _t->on_pushButton_close_clicked(); break;
        case 18: _t->on_pushButton_tibia_clicked(); break;
        case 19: _t->on_pushButton_femur_clicked(); break;
        case 20: _t->on_pushButton_step_1_clicked(); break;
        case 21: _t->on_pushButton_step_2_clicked(); break;
        case 22: _t->on_pushButton_step_3_clicked(); break;
        case 23: _t->on_pushButton_step_4_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject NavarQT::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_NavarQT.data,
      qt_meta_data_NavarQT,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *NavarQT::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *NavarQT::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_NavarQT.stringdata0))
        return static_cast<void*>(const_cast< NavarQT*>(this));
    return QWidget::qt_metacast(_clname);
}

int NavarQT::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 24)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 24;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
